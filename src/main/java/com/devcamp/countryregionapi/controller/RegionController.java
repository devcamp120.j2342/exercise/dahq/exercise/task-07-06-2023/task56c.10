package com.devcamp.countryregionapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionapi.model.Region;
import com.devcamp.countryregionapi.service.RegionService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class RegionController {
    @Autowired
    private RegionService regionService;

    @GetMapping("/region-info")
    public Region findRegion(@RequestParam(required = true, name = "code") String regionCode) {
        Region regionResult = regionService.findRegion(regionCode);
        return regionResult;
    }

}
