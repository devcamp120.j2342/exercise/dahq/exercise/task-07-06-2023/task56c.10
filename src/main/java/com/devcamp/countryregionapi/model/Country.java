package com.devcamp.countryregionapi.model;

import java.util.ArrayList;

public class Country {
    private String countryCode;
    private String countryName;
    private ArrayList<Region> regions;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public ArrayList<Region> getRegions() {
        return regions;
    }

    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }

    public Country() {
    }

    public Country(String countryCode, String countryName, ArrayList<Region> regions) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.regions = regions;
    }

    public Country(String countryCode, String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    @Override
    public String toString() {
        return "Country [countryCode=" + countryCode + ", countryName=" + countryName + ", regions=" + regions + "]";
    }

}
