package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Region;

@Service
public class RegionService {
    Region hcm = new Region("HCM", "HoChiMinh");
    Region hn = new Region("HN", "Hanoi");
    Region hp = new Region("HP", "HaiPhong");
    Region dn = new Region("DN", "DaNang");
    Region ny = new Region("NY", "NewYork");
    Region lv = new Region("LV", "LasVegas");
    Region sn = new Region("SN", "Sydney");
    Region mel = new Region("Mel", "Melbourne");
    Region tor = new Region("TOR", "Toronto");
    Region vic = new Region("VIC", "Victoria");
    Region tok = new Region("TOK", "Tokyo");
    Region kobe = new Region("KOBE", "Kobe");
    Region th = new Region("TH", "ThuongHai");
    Region tk = new Region("TK", "TrungKhanh");

    public ArrayList<Region> regionsVietNam() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(hcm);
        regions.add(hn);
        regions.add(hp);
        regions.add(dn);
        return regions;

    }

    public ArrayList<Region> regionsUSA() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(ny);
        regions.add(lv);

        return regions;

    }

    public ArrayList<Region> regionsUs() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(sn);
        regions.add(mel);

        return regions;

    }

    public ArrayList<Region> regionsCanada() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(tor);
        regions.add(vic);

        return regions;

    }

    public ArrayList<Region> regionsJapan() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(tok);
        regions.add(kobe);

        return regions;

    }

    public ArrayList<Region> regionsTQ() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(th);
        regions.add(tk);

        return regions;

    }

    public Region findRegion(String paramCode) {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(hcm);
        regions.add(hn);
        regions.add(hp);
        regions.add(dn);
        regions.add(ny);
        regions.add(lv);
        regions.add(sn);
        regions.add(mel);
        Region region = new Region();
        for (Region re : regions) {
            if (re.getRegionCode().equals(paramCode)) {
                region = re;
            }
        }
        return region;
    }

    public ArrayList<Region> allRegion(String paramCode) {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(hcm);
        regions.add(hn);
        regions.add(hp);
        regions.add(dn);
        regions.add(ny);
        regions.add(lv);
        regions.add(sn);
        regions.add(mel);
        regions.add(tor);
        regions.add(vic);
        regions.add(tok);
        regions.add(kobe);
        regions.add(th);
        regions.add(tk);
        return regions;
    }

}
